import React from "react";
import Option from "./Option";


const Options = (props) => (
    <div>
        <button onClick={props.deleteOptions}>Remove All</button>
        {props.options.length === 0 && (<p>Please add an option to get started!</p>)}
        {
            props.options.map((option, indx) => (
                <Option
                    key={indx}
                    optionText={option}
                    deleteSingleOption={props.deleteSingleOption}
                />
            ))
        }
    </div>
);

export default Options;