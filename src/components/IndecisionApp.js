import React from 'react';

import AddOption from "./AddOption";
import Action from "./Action";
import Header from "./Header";
import Options from "./Options";
import OptionModal from "./OptionModal";


class IndecisionApp extends React.Component {
    state = {
        options: [],
        selectedOption: undefined
    }

    deleteOptions = () => {
        this.setState(() => ({ options: [] }));
    }

    deleteSingleOption = (optionToRemove) => {
        this.setState((prevState) => ({
            options: prevState.options.filter(
                (option) => (option !== optionToRemove)
            )})
        );
    }

    handlePick = () => {
        const chosen_index = Math.floor(this.state.options.length * Math.random());
        this.setState(() => ({
                selectedOption: this.state.options[chosen_index]
            }));
    }

    clearSelectedOption = () => {
        this.setState(() => ({selectedOption: undefined}));
    }

    addOption = (option) => {
        if (!option) {
            return "Enter valid option";
        } else if (this.state.options.indexOf(option) > -1) {
            return "This option already exists";
        }
        this.setState((prevState) => ({ options: prevState.options.concat(option) }));
    }

    componentDidMount() {
        try {
            const json = localStorage.getItem('options');
            const options = JSON.parse(json);

            if (options) this.setState(() => ({ options }));
        }
        catch(e) {
            // Do nothing at all
        }
    }

    componentDidUpdate(_, prevState) {
        if (prevState.options.length !== this.state.options.length){
            const json = JSON.stringify(this.state.options);
            localStorage.setItem('options', json);
        }
    }

    render() {
        const title = 'Indesicion App';
        const subtitle = 'Put your life in the hands of a computer';

        return (
            <div>
                <Header title={title} subtitle={subtitle}/>
                <Action
                    hasOptions={this.state.options.length > 0}
                    handlePick={this.handlePick}
                />
                <Options
                    options={this.state.options}
                    deleteOptions={this.deleteOptions}
                    deleteSingleOption={this.deleteSingleOption}
                />
                <AddOption
                    addOption={this.addOption}
                />
                <OptionModal
                    selectedOption={this.state.selectedOption}
                    clearSelectedOption={this.clearSelectedOption}
                />
            </div>
        );
    }
}

export default IndecisionApp;