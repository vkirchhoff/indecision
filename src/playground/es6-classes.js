class Person {
    constructor(name = "John Doe", age = 0) {
        this.name = name;
        this.age = age;
    }
    say_hi() {
        return `Hi I am ${this.name}. ${this.name} is ${this.age} years old`;
    }
}

class Student extends Person {
    constructor(name, age, major = '') {
        super(name, age);
        this.major = major;
    }

    hasMajor() {
        return !!this.major;
    }

    say_hi() {
        let greeting = super.say_hi();
        if (this.hasMajor()) greeting = greeting + `. I am a bachelor of ${this.major}`;
        return greeting;
    }
}

const me = new Student("Fedor Kravchenko", 26, 'Physics');
console.log(me.say_hi());
const anon = new Person();
console.log(anon.say_hi());
