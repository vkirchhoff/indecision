class Counter extends React.Component {
    constructor(props) {
        super(props);
        this.addOne = this.addOne.bind(this);
        this.substructOne = this.substructOne.bind(this);
        this.reset = this.reset.bind(this);
        this.state = {
            count: props.count
        }
    }

    componentDidMount() {
        try {
            const raw_count = localStorage.getItem('count');
            const count = parseInt(raw_count);

            if (count) this.setState(() => ({ count }));
        }
        catch(e) {
            // Do nothing at all
        }
    }

    componentDidUpdate(_, prevState) {
        if (prevState.count !== this.state.count){
            localStorage.setItem('count', this.state.count);
        }
    }

    addOne() {
        this.setState((prevState) => {
            return {
                count: prevState.count + 1
            }
        });
    }

    substructOne() {
        this.setState((prevState) => {
            return {
                count: prevState.count - 1
            }
        });
    }
    reset () {
        this.setState(() => {
            return {
                count: 0
            }
        });
    }

    render() {
        return (
            <div>
                <h1>Count: { this.state.count }</h1>
                <button onClick={this.addOne}>+1</button>
                <button onClick={this.substructOne}>-1</button>
                <button onClick={this.reset}>reset</button>
            </div>
        );
    }
}

Counter.defaultProps = {
    count: 0
};

ReactDOM.render(<Counter/>, document.getElementById('app'));
