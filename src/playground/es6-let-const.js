const multiplier = {
    numbers: [2, 4, 6, 8],
    multiply_by: 2,
    multiply() {
        return this.numbers.map((number) => number * this.multiply_by);
    }
}

console.log(multiplier.multiply())