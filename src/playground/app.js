// class IndecisionApp extends React.Component {
//     constructor(props) {
//         super(props);
//         this.deleteOptions = this.deleteOptions.bind(this);
//         this.deleteSingleOption = this.deleteSingleOption.bind(this);
//         this.handlePick = this.handlePick.bind(this);
//         this.addOption = this.addOption.bind(this);
//         this.state = {
//             options: props.options
//         }
//     }
//
//     componentDidMount() {
//         try {
//             const json = localStorage.getItem('options');
//             const options = JSON.parse(json);
//
//             if (options) this.setState(() => ({ options }));
//         }
//         catch(e) {
//             // Do nothing at all
//         }
//     }
//
//     componentDidUpdate(_, prevState) {
//         if (prevState.options.length !== this.state.options.length){
//             const json = JSON.stringify(this.state.options);
//             localStorage.setItem('options', json);
//         }
//     }
//
//     deleteOptions() {
//         this.setState(() => ({ options: [] }));
//     }
//
//     deleteSingleOption(optionToRemove) {
//         this.setState((prevState) => ({
//                     options: prevState.options.filter(
//                         (option) => (option !== optionToRemove)
//                     )})
//         );
//     }
//
//     handlePick() {
//         const chosen_index = Math.floor(this.state.options.length * Math.random());
//         alert(
//             this.state.options[chosen_index]
//         );
//     }
//
//     addOption(option) {
//         if (!option) {
//             return "Enter valid option";
//         } else if (this.state.options.indexOf(option) > -1) {
//             return "This option already exists";
//         }
//         this.setState((prevState) => ({ options: prevState.options.concat(option) }));
//     }
//
//     render() {
//         const title = 'Indesicion App';
//         const subtitle = 'Put your life in the hands of a computer';
//
//         return (
//             <div>
//                 <Header title={title} subtitle={subtitle}/>
//                 <Action
//                     hasOptions={this.state.options.length > 0}
//                     handlePick={this.handlePick}
//                 />
//                 <Options
//                     options={this.state.options}
//                     deleteOptions={this.deleteOptions}
//                     deleteSingleOption={this.deleteSingleOption}
//                 />
//                 <AddOption
//                     addOption={this.addOption}
//                 />
//             </div>
//         );
//     }
// }
//
// const Header = (props) => {
//     return (
//         <div>
//             <h1>{props.title}</h1>
//             <h2>{props.subtitle}</h2>
//         </div>
//     );
// }
//
// const Action = (props) => {
//     return (
//         <div>
//             <button disabled={!props.hasOptions} onClick={props.handlePick}>
//                 What should I do?
//             </button>
//         </div>
//     );
// }
//
// const Options = (props) => {
//     return (
//       <div>
//           <button onClick={props.deleteOptions}>Remove All</button>
//           {props.options.length === 0 && (<p>Please add an option to get started!</p>)}
//           {
//               props.options.map((option, indx) => (
//                   <Option
//                       key={indx}
//                       optionText={option}
//                       deleteSingleOption={props.deleteSingleOption}
//                   />
//               ))
//           }
//       </div>
//     );
// }
//
// const Option = (props) => {
//     return (
//         <div key={props.key}>
//             {props.optionText}
//             <button
//                 onClick={(e) => {
//                     props.deleteSingleOption(props.optionText)
//                 }}
//             >
//                 remove
//             </button>
//         </div>
//     );
// }
//
// class AddOption extends React.Component {
//     constructor(props) {
//         super(props);
//         this.handleAddOption = this.handleAddOption.bind(this);
//         this.state = {
//             error: undefined
//         }
//     }
//     handleAddOption(e){
//         e.preventDefault();
//         const option = e.target.option.value.trim();
//         const error = this.props.addOption(option);
//         this.setState(() => ({ error }));
//         if (!error) e.target.option.value = '';
//     }
//
//     render() {
//         return (
//             <div>
//                 {this.state.error && <p>{this.state.error}</p>}
//                 <form onSubmit={this.handleAddOption}>
//                     <input type="text" name="option"/>
//                     <button>Add Option</button>
//                 </form>
//             </div>
//         );
//     }
// }
//
// IndecisionApp.defaultProps = {
//     options: []
// }
//
//
//
// ReactDOM.render(<IndecisionApp />, document.getElementById('app'))