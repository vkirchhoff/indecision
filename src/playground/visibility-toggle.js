class VisibilityToggle extends React.Component {
    constructor(props) {
        super(props);
        this.toggle = this.toggle.bind(this);
        this.state = {
            visibility: false
        }
    }

    toggle() {
        this.setState((prevState) => {
            return {
                visibility: !prevState.visibility
            }
        });
    }

    render() {
        return (
            <div>
                <h1>VisibilityToggle</h1>
                <button onClick={this.toggle}>
                    {this.state.visibility ? "Hide text": "Toggle text"}
                </button>
                {
                    this.state.visibility &&
                    (<p>Toggled text here</p>)
                }
            </div>
        );
    }
}

ReactDOM.render(<VisibilityToggle/>, document.getElementById('app'));