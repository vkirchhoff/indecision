console.log('App.js is running!');

// JSX - JavaScript XML

const app = {
    title: "Indecision App",
    subtitle: "Put your life in the hands of a computer",
    options: []
}

const onFormSubmit = (e) => {
    e.preventDefault();
    const option = e.target.elements.option.value;
    if (option) {
        app.options.push(option);
        e.target.elements.option.value = '';
        renderApp();
    }
}

const removeAll = (e) => {
    app.options = [];
    renderApp();
}

const makeDecision = (e) => {
    const chosen_idx = Math.floor(Math.random() * app.options.length);
    alert(app.options[chosen_idx]);
}

let appRoot = document.getElementById('app');

const renderApp = () => {
    let template = (
        <div>
            <h1>{app.title}</h1>
            {app.subtitle && <p>{app.subtitle}</p>}
            <p>{app.options.length > 0 ? "Here are some options": "No options found"}</p>
            <button disabled={app.options.length === 0} onClick={makeDecision}>What should I do?</button>
            <button onClick={removeAll}>Remove All</button>
            <ol>
                {app.options.map((opt, idx) => <li key={idx}>{opt}</li>)}
            </ol>
            <form onSubmit={onFormSubmit}>
                <input type="text" name="option"/>
                <button>Add Option</button>
            </form>
        </div>
    );

    ReactDOM.render(template, appRoot);
}
renderApp();